use std::collections::HashMap;
use std::fs;
use std::time::Instant;

type Generated = Vec<String>;

fn generate(input: &str) -> Generated {
    input
        .lines()
        .map(|l| l.trim())
        .filter(|l| !l.is_empty())
        .map(|l| l.parse().unwrap())
        .collect()
}

fn part_1(input: &Generated) -> i32 {
    input
        .into_iter()
        .map(|line| {
            let first_digit = line.chars().find(|char| char.is_numeric()).unwrap();
            let last_digit = line.chars().rfind(|char| char.is_numeric()).unwrap();

            let number: i32 = format!("{first_digit}{last_digit}").parse().unwrap();

            number
        })
        .sum()
}

fn scan(line: &str) -> Vec<(usize, String)> {
    let list = [
        "one", "1", "two", "2", "three", "3", "four", "4", "five", "5", "six", "6", "seven", "7",
        "eight", "8", "nine", "9",
    ];
    let replace_map: HashMap<&str, &str> = HashMap::from([
        ("one", "1"),
        ("two", "2"),
        ("three", "3"),
        ("four", "4"),
        ("five", "5"),
        ("six", "6"),
        ("seven", "7"),
        ("eight", "8"),
        ("nine", "9"),
    ]);

    let mut results: Vec<(usize, String)> = Vec::new();

    list.iter().cloned().for_each(|item| {
        let indices: Vec<_> = line.match_indices(item).collect();

        for (index, item) in indices {
            let value_str = if let Some(&replacement) = replace_map.get(item) {
                replacement
            } else {
                item
            };
            results.push((index, value_str.to_owned()))
        }
    });

    results
}

fn part_2(input: &Generated) -> i32 {
    let scanned: Vec<_> = input
        .into_iter()
        .map(|line| {
            let mut res = scan(line);
            res.sort_by(|item1, item2| item1.0.cmp(&item2.0));
            res
        })
        .collect();

    scanned
        .into_iter()
        .map(|scan| {
            let first_digit = &scan.first().unwrap().1;
            let last_digit = &scan.last().unwrap().1;

            let number: i32 = format!("{first_digit}{last_digit}").parse().unwrap();

            number
        })
        .sum()
}

fn main() {
    let content = fs::read_to_string("input").expect("file not found");

    let data = generate(&content);

    let res1_start = Instant::now();
    let res1 = part_1(&data);
    let res1_stop = Instant::now();

    let res2_start = Instant::now();
    let res2 = part_2(&data);
    let res2_stop = Instant::now();

    print!(
        "Result1: {}\nResolved in: {:?}\n",
        res1,
        res1_stop.duration_since(res1_start)
    );
    print!(
        "Result2: {}\nResolved in: {:?}\n",
        res2,
        res2_stop.duration_since(res2_start)
    );
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_part_1() {
        assert_eq!(
            142,
            part_1(&generate(
                &"1abc2
        pqr3stu8vwx
        a1b2c3d4e5f
        treb7uchet"
            ))
        )
    }
    #[test]
    fn test_part_2() {
        assert_eq!(
            281,
            part_2(&generate(
                &"two1nine
        eightwothree
        abcone2threexyz
        xtwone3four
        4nineeightseven2
        zoneight234
        7pqrstsixteen"
            ))
        )
    }

    #[test]
    fn test_part_2_1() {
        assert_eq!(
            54,
            part_2(&generate(&"fivefourdfjjpv85fourkcttlqdpksxqjzgbgk"))
        )
    }
}
